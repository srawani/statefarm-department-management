package com.statefarm.database.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Department")
public class DepartmentEntity extends BaseModel{

	
	private static final long serialVersionUID = -3378069356258668956L;
	
	@Column(name="DEPARTMENT_NAME")
	private String name;
	
	@OneToMany(mappedBy="department")
	private List<EmployeeEntity> employeeEntity;
	public List<EmployeeEntity> getEmployeeEntity() {
		return employeeEntity;
	}

	public void addEmployeeEntity(EmployeeEntity employeeEntity) {
		this.employeeEntity.add(employeeEntity);
	}
	
	public void deleteEmployeeEntity(EmployeeEntity employeeEntity) {
		this.employeeEntity.remove(employeeEntity);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DepartmentEntity() {
		
	}

	public DepartmentEntity(long id, String name) {
		super(id);
		this.name = name;
	}

	

}
