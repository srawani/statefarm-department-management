package com.statefarm.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Employee")
public class EmployeeEntity  extends BaseModel {

	
	private static final long serialVersionUID = 2219904362927966592L;

	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private DepartmentEntity department;
	
	public DepartmentEntity getDepartment() {
		return department;
	}
	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}
	public EmployeeEntity() {
		
	}
	public EmployeeEntity(long id, String firstName,String middleName,String lastName) {
		
		 super(id);
		this.firstName = firstName;
		this.middleName=middleName;
		this.lastName=lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	 
}
