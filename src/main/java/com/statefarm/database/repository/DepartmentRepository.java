package com.statefarm.database.repository;

import java.util.List;

import com.statefarm.database.entity.DepartmentEntity;

public interface DepartmentRepository {

	public DepartmentEntity get(long id);
	public List<DepartmentEntity> getAll();
	public DepartmentEntity post(DepartmentEntity department);
	public DepartmentEntity put(long id, DepartmentEntity department);
	public long delete(long id);
}
