package com.statefarm.database.repository;

import java.util.List;

import com.statefarm.database.entity.EmployeeEntity;

public interface EmployeeRepository {

	public EmployeeEntity get(long id);
	public List<EmployeeEntity> getAll();
	public EmployeeEntity post(EmployeeEntity employee);
	public EmployeeEntity put(long id, EmployeeEntity employee);
	public long delete(long id);
	
}
