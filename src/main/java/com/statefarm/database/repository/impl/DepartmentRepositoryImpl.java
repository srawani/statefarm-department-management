package com.statefarm.database.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.statefarm.database.entity.DepartmentEntity;
import com.statefarm.database.repository.DepartmentRepository;

@Repository
@Transactional
public class DepartmentRepositoryImpl implements DepartmentRepository {

		@Autowired
		EntityManager em;

		@Override
		public DepartmentEntity get(long id) {
			return em.find(DepartmentEntity.class, id);
		}

		@Override
		public List<DepartmentEntity> getAll() {
			return em.createQuery("select E from DepartmentEntity E", DepartmentEntity.class)
					.getResultList();

		}

		@Override
		public DepartmentEntity post(DepartmentEntity department) {
			em.persist(department);
			return department;
		}

		

		@Override
		public long delete(long id) {
			em.remove(get(id));
			return id;
		}

		@Override
		public DepartmentEntity put(long id, DepartmentEntity department) {
			return em.merge(department);
		}

		

}
