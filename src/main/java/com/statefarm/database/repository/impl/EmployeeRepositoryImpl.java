package com.statefarm.database.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.statefarm.database.entity.EmployeeEntity;
import com.statefarm.database.repository.EmployeeRepository;

@Repository
@Transactional
public class EmployeeRepositoryImpl implements EmployeeRepository {

	@Autowired
	EntityManager em;

	@Override
	public EmployeeEntity get(long id) {
		return em.find(EmployeeEntity.class, id);
	}

	@Override
	public List<EmployeeEntity> getAll() {
		return em.createQuery("select E from EmployeeEntity E", EmployeeEntity.class)
				.getResultList();

	}

	@Override
	public EmployeeEntity post(EmployeeEntity employee) {
		em.persist(employee);
		return employee;
	}

	@Override
	public EmployeeEntity put(long id, EmployeeEntity employee) {
		return em.merge(employee);

	}

	@Override
	public long delete(long id) {
		em.remove(get(id));
		return id;
	}

}
