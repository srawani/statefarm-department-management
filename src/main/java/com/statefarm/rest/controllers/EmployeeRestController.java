package com.statefarm.rest.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.statefarm.services.EmployeeService;
import com.statefarm.services.models.Employee;

@RestController
@RequestMapping("api/employee")
public class EmployeeRestController implements RController {

	@Autowired
	private EmployeeService es;

	 @RequestMapping(method = RequestMethod.GET)
	public List<Employee> getAll() throws IOException {
		return es.getAll();

	}

	@RequestMapping(method = RequestMethod.POST)
	public Employee post(@RequestBody Employee employee) {
		return es.post(employee);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Employee put(@PathVariable(value = "id") long id, @RequestBody Employee employee) {
		return es.put(id,employee);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public long delete(@PathVariable(value = "id") long id) {
		return es.delete(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Employee get(@PathVariable(value = "id") long id) {
		return es.get(id);
	}

}
