package com.statefarm.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.statefarm.services.DepartmentService;
import com.statefarm.services.models.Department;

@RestController
@RequestMapping("api/department")
public class DepartmentRestController implements RController {

	@Autowired
	private DepartmentService ds;


	@RequestMapping(method = RequestMethod.GET)
	public List<Department> getAll() {
		return ds.getAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Department post(@RequestBody Department department) {
		return ds.post(department);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Department put(@PathVariable(value = "id") long id, @RequestBody Department department) {
		return ds.put(id, department);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public long delete(@PathVariable(value = "id") long id) {
		return ds.delete(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Department get(@PathVariable(value = "id") long id) {
		return ds.get(id);
	}
}
