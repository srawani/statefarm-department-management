package com.statefarm.services;

import java.io.IOException;
import java.util.List;

import com.statefarm.services.models.Employee;

public interface EmployeeService {

	public List<Employee> getAll() throws IOException;

	public Employee post(Employee employee);

	public Employee put(long id, Employee employee);

	public long delete(long id);

	public Employee get(long id) ;
	

}
