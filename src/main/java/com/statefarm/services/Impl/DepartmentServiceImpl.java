package com.statefarm.services.Impl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.statefarm.database.entity.DepartmentEntity;
import com.statefarm.database.repository.DepartmentRepository;
import com.statefarm.services.DepartmentService;
import com.statefarm.services.models.Department;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository dr;

	public List<Department> getAll() {
		return dr.getAll().stream().map(translate).collect(Collectors.toList());
	}

	public Department post(Department department) {
		return translate.apply(dr.post(translateToEntity.apply(department)));
	}

	public Department put(long id, @RequestBody Department department) {
		return translate.apply(dr.put(id, translateToEntity.apply(department)));
	}

	public long delete(long id) {
		return dr.delete(id);
	}

	public Department get(long id) {
		return translate.apply(dr.get(id));
	}

	private static Function<DepartmentEntity, Department> translate = (x) -> new Department(x.getId(), x.getName());
	private static Function<Department, DepartmentEntity> translateToEntity = (x) -> new DepartmentEntity(x.getId(),
			x.getName());
}
