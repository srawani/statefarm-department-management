package com.statefarm.services.Impl;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.statefarm.database.entity.EmployeeEntity;
import com.statefarm.database.repository.EmployeeRepository;
import com.statefarm.services.EmployeeService;
import com.statefarm.services.models.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository er;

	public List<Employee> getAll() throws IOException {
		return er.getAll().stream().map(translate).collect(Collectors.toList());

	}

	public Employee post(Employee employee) {
		return translate.apply(er.post(translateToEntity.apply(employee)));
	}

	public Employee put(long id, Employee employee) {
		return translate.apply(er.put(id, translateToEntity.apply(employee)));
	}

	public long delete(long id) {
		return er.delete(id);
	}

	public Employee get(long id) {
		return translate.apply(er.get(id));
	}

	private static Function<EmployeeEntity, Employee> translate = (x) -> new Employee(x.getId(), x.getFirstName(),
			x.getMiddleName(), x.getLastName(), 0);
	private static Function<Employee, EmployeeEntity> translateToEntity = (x) -> new EmployeeEntity(x.getId(),
			x.getFirstName(), x.getMiddleName(), x.getLastName());

}
