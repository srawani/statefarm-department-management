package com.statefarm.services.models;

public class Department extends ServiceModel {

	private long id;	
	private String name;
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department() {
		
	}

	public Department(long id, String name) {
		this.name = name;
		this.id=id;
	}

		
}
