package com.statefarm.services.models;


public class Employee extends ServiceModel {

	private long id;	
	private String firstName;
	private String middleName;
	private String lastName;
	private long departmentId;

	
	public long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
	public Employee() {
		
	}
	public Employee(long id, String firstName,String middleName,String lastName,long departmentId) {
		
		this.id = id;
		this.firstName = firstName;
		this.middleName=middleName;
		this.lastName=lastName;
		this.departmentId=departmentId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", departmentId=" + departmentId + "]";
	}
	
	
	
	 
}
