package com.statefarm.services;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.statefarm.services.models.Department;

public interface DepartmentService {

	List<Department> getAll();

	Department post(Department department);

	Department put(long id, @RequestBody Department department);

	long delete(long id);

	Department get(long id);
}
