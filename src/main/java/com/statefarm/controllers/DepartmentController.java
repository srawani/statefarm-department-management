package com.statefarm.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.statefarm.services.DepartmentService;
import com.statefarm.services.models.Department;
import com.statefarm.services.models.Employee;

@org.springframework.stereotype.Controller

public class DepartmentController implements Controller {

	@Autowired
	private DepartmentService ds;

	@RequestMapping(method = RequestMethod.GET,value = "/department")
	public String findAll(Model model) {
		model.addAttribute("department", new Department());
		model.addAttribute("departments", ds.getAll());
		return "department";
	}

	@RequestMapping(method = RequestMethod.POST,value = "/department")
	public String post(@ModelAttribute("department") Department department, Model model) { 
				ds.post(department);
		return "department";
	}
	
	@GetMapping("/allDepartments")
	public String departments(Model model) throws IOException {
		model.addAttribute("departments1", ds.getAll());
		return "all-departments";
	}



}
