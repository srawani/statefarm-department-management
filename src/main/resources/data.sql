INSERT INTO Department(id,department_name) Values
(1,'CSE'),
(2,'IT'),
(3,'R&D');
INSERT INTO Employee(id,first_name,middle_name, last_name,department_id) VALUES
  (1,'Srawani', null, 'Atnoorkar',1),
  (2,'Bill', null,'Gates',2),
  (3,'Alex', 'J', 'Joseph',2);
