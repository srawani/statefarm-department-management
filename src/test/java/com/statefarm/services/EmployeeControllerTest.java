package com.statefarm.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.statefarm.rest.controllers.EmployeeRestController;
import com.statefarm.services.models.Employee;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class EmployeeControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private EmployeeService es;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testGet() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		mockMvc.perform(get("/api/employee/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id").value("1"))
				.andExpect(jsonPath("$.firstName").value("Srawani")).andExpect(jsonPath("$.middleName").isEmpty())
				.andExpect(jsonPath("$.lastName").value("Atnoorkar"));

	}

	@Test
	public void testGetAll() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		ObjectMapper obj = new ObjectMapper();

		MvcResult mvcResult = mockMvc.perform(get("/api/employee")).andReturn();

		int status = mvcResult.getResponse().getStatus();

		assertEquals(200, status);

		Employee[] e = obj.readValue(mvcResult.getResponse().getContentAsString(), Employee[].class);

		assertTrue(e.length > 0);

	}

	@Test
	public void testPost() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		Employee e = new Employee(4, "Srawani", null, "Atnoorkar", 1);

		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/employee")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"firstName\":\"Srawani\",\"middleName\":\"\",\"lastName\":\"Atnoorkar\",\"departmentId\":1}"))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();

		assertEquals(200, status);

	}

	@Test
	public void testDelete() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		int prevSize = es.getAll().size();

		MvcResult mvcResult = mockMvc.perform(delete("/api/employee/1")).andReturn();
		int status = mvcResult.getResponse().getStatus();

		int currSize = es.getAll().size();

		assertEquals(prevSize - 1, currSize);
	}

}
